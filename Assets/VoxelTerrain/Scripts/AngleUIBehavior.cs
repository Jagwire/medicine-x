﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AngleUIBehavior : MonoBehaviour {

	public Color32 UpperColor;
	public Color32 MiddleColor;
	public Color32 LowerColor;

	public float UpperAngle;
	public float MiddleAngle;
	public float LowerAngle;

	public MemoryWorkoutManager workoutMaganager;
	float currentAngle;

	bool balanced;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		currentAngle = (float)workoutMaganager.checkBalance ();

		if (currentAngle > UpperAngle) {
			GetComponent<Image>().color  = UpperColor;
			GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			balanced = false;
			return;
		}

		if(currentAngle < LowerAngle) {

			GetComponent<Image>().color  = LowerColor;
			GetComponent<RectTransform>().localScale = new Vector3(0.3f, 0.3f, 0.3f);
			balanced = true;
			return;
		}

		GetComponent<Image>().color  = MiddleColor;
		float radius = currentAngle / UpperAngle;
		GetComponent<RectTransform>().localScale = new Vector3 (radius, radius, radius);
		balanced = true;
	}

	public bool isBalanced ()
	{
		return balanced;
	}
}

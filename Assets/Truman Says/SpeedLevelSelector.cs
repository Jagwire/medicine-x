﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeedLevelSelector : MonoBehaviour {

	public ParameterPreserver parameters;
	public Toggle leftToggle, rightToggle, bothToggle;

	public void loadLevelOne(){
		parameters.level = 1;
		
		if (bothToggle.isOn) {
			parameters.leftFoot = false;
			parameters.rightFoot = false;
		}
		else {
			parameters.leftFoot = leftToggle.isOn;
			parameters.rightFoot = rightToggle.isOn;
		}
		
		Application.LoadLevel (6);
	}
	
	public void loadLevelTwo(){
		parameters.level = 2;
		
		if (bothToggle.isOn) {
			parameters.leftFoot = false;
			parameters.rightFoot = false;
		}
		else {
			parameters.leftFoot = leftToggle.isOn;
			parameters.rightFoot = rightToggle.isOn;
		}
		
		Application.LoadLevel (6);
	}

	public void back(){
		Application.LoadLevel (2);
	}
}

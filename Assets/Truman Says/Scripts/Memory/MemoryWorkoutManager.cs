﻿using UnityEngine;
using System.Collections;

public class MemoryWorkoutManager : MonoBehaviour {
	ParameterPreserver parameters;
	int level;
	bool penaltyOn;

	public Transform FootLeftTransform, FootRightTransform, FootBothTransform;
	public Transform SpineBaseTransform, SpineShoulderTransform;
	public GameObject square1, square2, square3, square4;
	public MemoryInstructionUpdate memoryInstructionUpdate;
	public SpotlightManager spotlight;
	public AngleHoop angleUI;
	public Transform speedTether;

	//for setting y-value to that of resting foot height
	public Transform squares;

	public double jumpThreshold;
	double resting = 0;
	bool jumped;

	public int numInstructions = 20;
	int currentInstruction;
	string instruction;
	int progress = 0;
	int currentScore = 0;
	static int totalJumps = 0;

	GameObject square, lastSquare;
	bool complete;
	bool running;

	string[,] workout;

	Vector3 speedLocation, lastSpeedLocation;
	public double speedThreshold;

	void Awake ()
	{
		parameters = GameObject.FindWithTag("MemoryParameters").GetComponent<ParameterPreserver>();
		level = parameters.level;
		penaltyOn = !parameters.safe;

		workout = new string[numInstructions, 2];

		squareToggle (false);
		jumped = false;

		speedLocation = speedTether.position;

		running = true;
		complete = false;
		currentInstruction = 2;
		generateWorkout();
	}
	
	void Update ()
	{

		if(progress == currentInstruction + 1)
		{
			running = false;
			squareToggle(false);
			progress = 0;
			currentInstruction++;
			memoryInstructionUpdate.setCountdownStarted(false);
			memoryInstructionUpdate.setInstructionGiven(false);
		}
		else if(running)
		{
			if(!complete)
			{
				complete = checkComplete ();
			}
			else if (angleUI.isBalanced ())
			{
				complete = false;
				progress++;
				currentScore++;
				totalJumps++;

                //illuminate on correct
                spotlight.illuminateWhile(square, true);
                square.GetComponent<AudioSource>().Play();
            }
		}
		
	}

	void generateWorkout()
	{
		string[] squares = {"Square1", "Square2", "Square3", "Square4"};
		string[] feet = {"FootLeft", "FootRight"};

		int previous = (int)Random.Range(0,4);
		for(int i = 0; i < numInstructions; i++)
		{
			previous = nextSquare(previous);
			workout[i, 0] = squares[previous];

			if(parameters.leftFoot)
				workout[i, 1] = "FootLeft";
			else if(parameters.rightFoot)
				workout[i, 1] = "FootRight";
			else
				workout[i, 1] = "FootBoth";
		}
	}

	int nextSquare(int previous)
	{
		int[] possible1 = {1, 3, 2}, possible2 = {0, 2, 3}, possible3 = {1, 3, 0}, possible4 = {0, 2, 1};

		switch (previous) 
		{
			case 0:
				return possible1 [(int)Random.Range (0, level + 1)];
			case 1:
				return possible2 [(int)Random.Range (0, level + 1)];
			case 2:
				return possible3 [(int)Random.Range (0, level + 1)];
			case 3:
				return possible4 [(int)Random.Range (0, level + 1)];
			default:
				return 0;
		}
	}

	public void deliverInstructions(int time, int number)
	{
		if(number < numInstructions)
		{
			string[] squares = new string[number];

			for (int i = 0; i < number; i++)
			{
				squares[i] = workout[i,0];
			}

			spotlight.illuminateSequence (squares, time);
		}
	}


	//check if instruction has been completed
	bool checkComplete()
	{
		//check if all instructions have been completed
		if(currentInstruction != numInstructions)
		{

			//get the square that pertains to current instruction
			if(progress == 0)
				lastSquare = GameObject.FindGameObjectWithTag(workout[progress, 0]);
			else
				lastSquare = GameObject.FindGameObjectWithTag(workout[progress - 1, 0]);;

			square = GameObject.FindGameObjectWithTag(workout[progress, 0]);


			if(jumped || jumpCheck(GameObject.FindGameObjectWithTag(workout[progress, 1])))
			{
				jumped = true;

				if(checkSpeed() > speedThreshold)
					return false;

				GameObject currentSquare = getCurrentSquare(workout[progress, 1]);
				
				if(currentSquare.tag == square.tag)
				{
					jumped = false;
					return true;
				}
				else if(currentSquare.tag == lastSquare.tag)
				{ 
					jumped = false;
					return false;
				}
				else if(currentSquare != gameObject)
				{
					jumped = false;
					if(penaltyOn)
						memoryInstructionUpdate.setIncorrectSquare(true);
					//Application.LoadLevel (Application.loadedLevelName);
					return false;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	//return current instruction
	public string getInstruction()
	{
		if(currentInstruction < numInstructions)
		{
			return workout[currentInstruction, 1] + " " + workout[currentInstruction, 0];
		}
		else
		{
			return "Done";
		}
	}

	public string getInstruction(int num)
	{
		if(currentInstruction < numInstructions)
		{
			return workout[num, 0];
		}
		else
		{
			return "Done";
		}
	}

	//set resting height to average of the y-value of both feet
	public void setRestingFootHeight()
	{
		resting = (FootLeftTransform.position.y + FootRightTransform.position.y) / 2;

		//align feet with squares
		Vector3 align = squares.position;
		align.y = (float)resting - .5f;
		squares.position = align;
	}

	//check if given foot has surpassed the jump threshold
	public bool jumpCheck(GameObject foot)
	{
		if (foot.tag == "FootLeft") {
			if (FootLeftTransform.position.y > resting + jumpThreshold) {
				return true;
			}
		}
		else if (foot.tag == "FootRight") {
			if (FootRightTransform.position.y > resting + jumpThreshold) {
				return true;
			}
		}
		else if (foot.tag == "FootBoth"){
			if (FootBothTransform.position.y > resting + jumpThreshold) {
				return true;
			}
		}

		return false;
	}

	//return integer of degrees from vertical
	public int checkBalance ()
	{
		Vector3 upper = SpineShoulderTransform.position;
		Vector3 lower = SpineBaseTransform.position;

		Vector3 actual = upper - lower;
		Vector3 unit_y = new Vector3(0, 1, 0);

		return (int)Vector3.Angle(actual, unit_y);
	}

	public float checkSpeed ()
	{
		lastSpeedLocation = speedLocation;
		speedLocation = speedTether.position;

		float distance = Mathf.Sqrt (Mathf.Pow (speedLocation.x - lastSpeedLocation.x, 2) + Mathf.Pow (speedLocation.y - lastSpeedLocation.y, 2) + Mathf.Pow (speedLocation.z - lastSpeedLocation.z, 2));
	
		return distance;
	}

	public GameObject getCurrentSquare(string foot)
	{
		if (foot == "FootLeft")
		{
			if (square1.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square1;

			if (square2.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square2.transform.position.y < jumpThreshold + .5f))
				return square2;

			if (square3.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square3.transform.position.y < jumpThreshold + .5f))
				return square3;

			if (square4.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square4.transform.position.y < jumpThreshold + .5f))
				return square4;
		}
		else if (foot == "FootRight")
		{
			if (square1.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square1;
			
			if (square2.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square2;

			if (square3.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square3;
			
			if (square4.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square4;
		}
		else if(foot == "FootBoth")
		{
			GameObject leftSquare = getCurrentSquare("FootLeft");
			GameObject rightSquare = getCurrentSquare("FootRight");

			if(leftSquare.tag == rightSquare.tag)
				return leftSquare;
		}

		return gameObject;
	}

	public void squareToggle(bool toggle)
	{
		square1.GetComponent<BoxCollider> ().enabled = toggle;
		square2.GetComponent<BoxCollider> ().enabled = toggle;
		square3.GetComponent<BoxCollider> ().enabled = toggle;
		square4.GetComponent<BoxCollider> ().enabled = toggle;
		if(toggle)
			running = true;
	}

	public double getResting()
	{
		return resting;
	}

	public int getCurrentInstruction()
	{
		return currentInstruction;
	}

	public void setCurrentInstruction(int newInstruction)
	{
		currentInstruction = newInstruction;
	}

	public int getProgress()
	{
		return progress;
	}

	public int getCurrentScore()
	{
		return currentScore;
	}

	public int getTotalJumps()
	{
		return totalJumps;
	}

}

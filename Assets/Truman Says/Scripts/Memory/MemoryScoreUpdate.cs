﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MemoryScoreUpdate : MonoBehaviour {

	public MemoryWorkoutManager workoutManager;

	Text text;
	// Use this for initialization
	void Awake ()
	{
		text = GetComponent <Text> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		text.text = "Round: " + (workoutManager.getCurrentInstruction() - 1).ToString() + "\n" + 
			"Score: " + workoutManager.getCurrentScore ().ToString() + "\n" +
			"Total Jumps: " + workoutManager.getTotalJumps().ToString();
	}
}

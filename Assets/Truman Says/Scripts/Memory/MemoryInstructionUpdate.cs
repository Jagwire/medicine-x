﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MemoryInstructionUpdate : MonoBehaviour
{
	public MemoryWorkoutManager workoutManager;
	public GameObject StartBox, Plane;
    ParameterPreserver parameters;

	public SpotlightManager spotlight;

	bool instructionGiven = false, instructionFinished = false;

    Text text;
	bool setTextActive;

    int timesRun;
	bool countdownStarted = false, countdownFinish = false;

	int instructionProgress = 0;

	bool incorrectSquare = false;


	void Awake ()
	{
        parameters = GameObject.FindWithTag("MemoryParameters").GetComponent<ParameterPreserver>();

        timesRun = 0;
        text = GetComponent <Text> ();

		setTextActive = false;
	}
	
	void Update ()
	{
		if (!countdownStarted)
		{
			if(incorrectSquare)
			{
				countdownStarted = true;
				countdownFinish = false;
				StartCoroutine(Countdown("Incorrect Square\nReseting: "));
				return;
			}
            text.color = Color.white;
			text.text = "Step in the Middle";

			if (StartBox.GetComponent<StartBoxManager> ().getInBox ()) 
			{
				countdownStarted = true;
				countdownFinish = false;

				if (timesRun == 0)
					StartCoroutine (Countdown ("Initializing: "));
				else
					StartCoroutine (Countdown ("Next Pattern: "));

				timesRun++;

			}
		}
		//after countdown, begin giving instructions
		else if(countdownFinish)
		{
			if(setTextActive)
				return;


			if(!instructionGiven)
			{
				instructionGiven = true;
				instructionFinished = false;
				workoutManager.deliverInstructions(2, workoutManager.getCurrentInstruction() + 1);
			}

			else if(instructionFinished)
			{
				text.color = Color.white;
                if (parameters.easy)
                {
                    switch (workoutManager.getInstruction(workoutManager.getProgress()))
                    {
                        case "Square1":
                            text.color = Color.red;
                            text.text = "Red";
                            break;
                        case "Square2":
                            text.color = Color.green;
                            text.text = "Green";
                            break;
                        case "Square3":
                            text.color = Color.yellow;
                            text.text = "Yellow";
                            break;
                        case "Square4":
                            text.color = Color.blue;
                            text.text = "Blue";
                            break;
                    }
                }
                else
                {
                    text.text = "Your Turn";
                }
			}
			else
			{
				//get current instruction (ex: FootRight Square3)
				string instruction = workoutManager.getInstruction(instructionProgress);

				//translate from GameObject tag naming convention to more understandable wording
				switch(instruction)
				{
					case "Square1":
						text.color = Color.red;
						text.text = "Red";
						break;
					case "Square2":
						text.color = Color.green;
						text.text = "Green";
						break;
					case "Square3":
						text.color = Color.yellow;
						text.text = "Yellow";
						break;
					case "Square4":
						text.color = Color.blue;
						text.text = "Blue";
						break;
				}
			}
		}

	}

	//count down for 5 seconds
	public IEnumerator Countdown(string newText)
	{
		Plane.SetActive (true);
		workoutManager.squareToggle (false);

		int seconds = 5;

		while(seconds > 0)
		{
			text.text = newText + seconds.ToString();
			seconds--;
			yield return new WaitForSeconds (1);

		}

		countdownFinish = true;
		text.text = "";
		Plane.SetActive (false);
		if(timesRun == 1)
			workoutManager.setRestingFootHeight();
		if(incorrectSquare)
		{
			Application.LoadLevel (Application.loadedLevelName);
		}
	}

	public void setText(string newText)
	{
		if (newText == "")
		{
			setTextActive = false;
			return;
		}

		setTextActive = true;
		text.text = newText;
	}

	public void setCountdownStarted(bool started)
	{
		countdownStarted = started;
	}

	public void setCountdownFinish(bool finished)
	{
		countdownFinish = finished;
	}

	public void setInstructionGiven (bool given)
	{
		instructionGiven = given;
	}

	public void setInstructionFinished (bool finished)
	{
		instructionFinished = finished;
	}

	public void setInstructionProgress (int progress)
	{
		instructionProgress = progress;
	}

	public void setIncorrectSquare (bool incorrect)
	{
		countdownStarted = !incorrect;
		incorrectSquare = incorrect;
	}
}

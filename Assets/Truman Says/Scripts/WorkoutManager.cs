﻿using UnityEngine;
using System.Collections;

public class WorkoutManager : MonoBehaviour {
	public Transform FootLeftTransform, FootRightTranform;
	public Transform SpineBaseTransform, SpineShoulderTransform;

	//for setting y-value to that of resting foot height
	public Transform squares;

	public double jumpThreshold;
	double resting = 0;

	public int numInstructions = 20;
	int currentInstruction;
	string instruction;

	string[,] workout;

	void Awake ()
	{
		workout = new string[numInstructions, 2];

		currentInstruction = 0;
		generateWorkout();
	}
	
	void Update ()
	{
		checkComplete();
	}

	void generateWorkout()
	{
		string[] squares = {"Square1", "Square2", "Square3", "Square4"};
		string[] feet = {"FootLeft", "FootRight"};

		int previous = 0;
		for(int i = 0; i < numInstructions; i++)
		{
			previous = nextSquare(previous);
			workout[i, 0] = squares[previous];

			//workout[i, 0] = squares[(int)Random.Range(0,4)];
			workout[i, 1] = feet[(int)Random.Range(0,1)];
		}
	}

	int nextSquare(int previous)
	{
		int[] possible1 = {1, 3}, possible2 = {0, 2}, possible3 = {1, 3}, possible4 = {0, 2};

		switch(previous)
		{
			case 0:
				return possible1[(int)Random.Range(0,2)];
			case 1:
				return possible2[(int)Random.Range(0,2)];
			case 2:
				return possible3[(int)Random.Range(0,2)];
			case 3:
				return possible4[(int)Random.Range(0,2)];
			default:
				return 0;
		}
	}

	//check if instruction has been completed
	void checkComplete()
	{
		//check if all instructions have been completed
		if(currentInstruction != numInstructions)
		{
			//get the square that pertains to current instruction
			SquareController square = GameObject.FindGameObjectsWithTag(workout[currentInstruction, 0])[0].GetComponent<SquareController>();

			//if left foot is required, is in the square, and jumped
			if(workout[currentInstruction, 1] == "FootLeft" && square.getLeftIn() && jumpCheck(GameObject.FindGameObjectsWithTag("FootLeft")[0]))
			{
				currentInstruction++;
			}
			//if right foot is required, is in the square, and jumped
			else if(workout[currentInstruction, 1] == "FootRight" && square.getRightIn() && jumpCheck(GameObject.FindGameObjectsWithTag("FootRight")[0]))
			{
				currentInstruction++;
			}
		}
	}

	//return current instruction
	public string getInstruction()
	{
		if(currentInstruction != numInstructions)
		{
			return workout[currentInstruction, 1] + " " + workout[currentInstruction, 0];
		}
		else
		{
			return "Done";
		}
	}

	//set resting height to average of the y-value of both feet
	public void setRestingFootHeight()
	{
		resting = (FootLeftTransform.position.y + FootRightTranform.position.y) / 2;

		//align feet with squares
		Vector3 align = squares.position;
		align.y = (float)resting - .5f;
		squares.position = align;
	}

	//check if given foot has surpassed the jump threshold
	public bool jumpCheck(GameObject foot)
	{
		//if no foot is provided, check for either
		if(foot == null)
		{
			if((FootLeftTransform.position.y > resting + jumpThreshold) || (FootRightTranform.position.y > resting + jumpThreshold))
			{
				return true;
			}
		}
		else if(foot.tag == "FootLeft")
		{
			if(FootLeftTransform.position.y > resting + jumpThreshold)
			{
				return true;
			}
		}
		else if(foot.tag == "FootRight")
		{
			if(FootRightTranform.position.y > resting + jumpThreshold)
			{
				return true;
			}
		}

		return false;
	}

	//return integer of degrees from vertical
	public int checkBalance ()
	{
		Vector3 upper = SpineShoulderTransform.position;
		Vector3 lower = SpineBaseTransform.position;

		Vector3 actual = upper - lower;
		Vector3 unit_y = new Vector3(0, 1, 0);

		return (int)Vector3.Angle(actual, unit_y);
	}

	public double getResting()
	{
		return resting;
	}
}

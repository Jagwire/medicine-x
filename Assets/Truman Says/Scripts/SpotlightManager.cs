﻿using UnityEngine;
using System.Collections;

public class SpotlightManager : MonoBehaviour {

	public MemoryInstructionUpdate instructionUpdate;
	public MemoryWorkoutManager workoutManager;

	Light myLight;
	bool finished;

	// Use this for initialization
	void Start ()
	{
		myLight = GetComponent<Light>();
		myLight.enabled = false;
		finished = true;
	}

	public void illuminateSequence(string [] squares, int time)
	{
		StartCoroutine (illuminateSequenceRoutine (squares, time));
	}

	IEnumerator illuminateSequenceRoutine(string [] squares, int time)
	{
		for(int i = 0; i < squares.Length; i++)
		{
			instructionUpdate.setInstructionProgress(i);
			illuminate (GameObject.FindGameObjectWithTag(squares[i]), time);
			yield return new WaitForSeconds(time);
		}

		myLight.enabled = false;

		instructionUpdate.setInstructionFinished (true);
		workoutManager.squareToggle (true);
	}


	public void illuminate(GameObject square, float time)
	{
		Vector3 squarePos = square.GetComponent<Transform> ().position;
		squarePos.y = transform.position.y;
		transform.position = squarePos;
		
		myLight.enabled = true;
		square.GetComponent<AudioSource> ().Play ();
	}

	public void illuminateWhile(GameObject square, bool state)
	{
		if (!finished)
			return;
		if (state)
		{
			Vector3 squarePos = square.GetComponent<Transform> ().position;
			squarePos.y = transform.position.y;
			transform.position = squarePos;

			myLight.enabled = true;
		}

		else
			myLight.enabled = false;
	}
}
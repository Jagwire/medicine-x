﻿using UnityEngine;
using System.Collections;

public class Appear : MonoBehaviour {

	public void Start()
	{
		gameObject.SetActive (false);
	}

	public void appear()
	{
		gameObject.SetActive (true);
	}

	public void disappear()
	{
		gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ParameterPreserver : MonoBehaviour {
	
	public bool leftFoot;
	public bool rightFoot;
	public bool safe;
    public bool easy;
	public int level;

	//COM parameters
	public bool gender; //male=true, female=false
	public int height;
	public int weight;

	// Use this for initialization
	void Start ()
	{
		DontDestroyOnLoad (this);
	}

}

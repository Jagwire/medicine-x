﻿using UnityEngine;
using System.Collections;

public class FootBothLocation : MonoBehaviour {

	public Transform FootLeft, FootRight;
	

	// Update is called once per frame
	void Update () {
		Vector3 leftLocation = FootLeft.position;
		Vector3 rightLocation = FootRight.position;
		Vector3 bothLocation;

		bothLocation.x = (leftLocation.x + rightLocation.x) / 2;
		bothLocation.y = (leftLocation.y + rightLocation.y) / 2;
		bothLocation.z = (leftLocation.z + rightLocation.z) / 2;

		gameObject.transform.position = bothLocation;
	}
}

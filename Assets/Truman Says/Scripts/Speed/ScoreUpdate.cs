﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreUpdate : MonoBehaviour {

	public SpeedWorkoutManager workoutManager;

	Text text;

	void Awake()
	{
		text = GetComponent <Text> ();
	}
	
	void FixedUpdate ()
	{
		text.text = "Jumps: " + workoutManager.getJumpCount().ToString();
	}
}

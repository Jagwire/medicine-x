﻿using UnityEngine;
using System.Collections;

public class SpeedWorkoutManager : MonoBehaviour {

	ParameterPreserver parameters;
	int level;

	public Transform FootLeftTransform, FootRightTransform, FootBothTransform;
	public Transform SpineBaseTransform, SpineShoulderTransform;
	public GameObject square1, square2, square3, square4;

	//for setting y-value to that of resting foot height
	public Transform squares;

	public double jumpThreshold;
	double resting;

	public int timer;
	bool timeFinish = false;
	int jumpCount;

	string instruction;
	int currentInstruction;

	string[,] workout;

	void Awake ()
	{
		parameters = GameObject.FindWithTag("SpeedParameters").GetComponent<ParameterPreserver>();
		level = parameters.level;

		workout = new string[2, 2];
		currentInstruction = 0;
		jumpCount = 0;
		resting = 0;

		generateWorkout();
	}
	
	void Update ()
	{
		checkComplete();
	}

	void generateWorkout()
	{
		if(level == 1)
		{
			workout[0,0] = "Square1";
			workout[1,0] = "Square2";
		}
		else if(level == 2)
		{
			workout[0,0] = "Square1";
			workout[1,0] = "Square4";
		}

		if(parameters.leftFoot)
		{
			workout[0, 1] = "FootLeft";
			workout[1, 1] = "FootLeft";
		}
		else if(parameters.rightFoot)
		{
			workout[0, 1] = "FootRight";
			workout[1, 1] = "FootRight";
		}
		else
		{
			workout[0, 1] = "FootBoth";
			workout[1, 1] = "FootBoth";
		}

	}

	//check if instruction has been completed
	void checkComplete()
	{
		//check if all instructions have been completed
		if(!timeFinish)
		{
			//get the square that pertains to current instruction
			SquareController square = GameObject.FindGameObjectWithTag(workout[currentInstruction, 0]).GetComponent<SquareController>();

			//if left foot is required, is in the square, and jumped
			if((getCurrentSquare(workout[currentInstruction,1]).tag == square.tag) && jumpCheck(GameObject.FindGameObjectWithTag(workout[currentInstruction,1])))
			{
				jumpCount++;
				currentInstruction++;
			}
		}

		if(currentInstruction == 2)
		{
			currentInstruction = 0;
		}
	}

	//return current instruction
	public string getInstruction()
	{
		return "Jump between\n" + workout[0, 0] + " and " + workout[1, 0];
	}

	//set resting height to average of the y-value of both feet
	public void setRestingFootHeight()
	{
		resting = (FootLeftTransform.position.y + FootRightTransform.position.y) / 2;

		//align feet with squares
		Vector3 align = squares.position;
		align.y = (float)resting;
		squares.position = align;
	}

	//check if given foot has surpassed the jump threshold
	bool jumpCheck(GameObject foot)
	{
		//if no foot is provided, check for either
		if(foot == null)
		{
			if((FootLeftTransform.position.y > resting + jumpThreshold) || (FootRightTransform.position.y > resting + jumpThreshold))
			{
				return true;
			}
		}
		else if(foot.tag == "FootLeft")
		{
			if(FootLeftTransform.position.y > resting + jumpThreshold)
			{
				return true;
			}
		}
		else if(foot.tag == "FootRight")
		{
			if(FootRightTransform.position.y > resting + jumpThreshold)
			{
				return true;
			}
		}
		else if(foot.tag == "FootBoth")
		{
			if(FootBothTransform.position.y > resting + jumpThreshold)
			{
				return true;
			}
		}

		return false;
	}

	//return integer of degrees from vertical
	public int checkBalance ()
	{
		Vector3 upper = SpineShoulderTransform.position;
		Vector3 lower = SpineBaseTransform.position;

		Vector3 actual = upper - lower;
		Vector3 unit_y = new Vector3(0, 1, 0);

		return (int)Vector3.Angle(actual, unit_y);
	}

	public GameObject getCurrentSquare(string foot)
	{
		if (foot == "FootLeft")
		{
			if (square1.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square1;
			
			if (square2.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square2.transform.position.y < jumpThreshold + .5f))
				return square2;
			
			if (square3.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square3.transform.position.y < jumpThreshold + .5f))
				return square3;
			
			if (square4.GetComponent<SquareController> ().getLeftIn () && (FootLeftTransform.position.y - square4.transform.position.y < jumpThreshold + .5f))
				return square4;
		}
		else if (foot == "FootRight")
		{
			if (square1.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square1;
			
			if (square2.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square2;
			
			if (square3.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square3;
			
			if (square4.GetComponent<SquareController> ().getRightIn () && (FootRightTransform.position.y - square1.transform.position.y < jumpThreshold + .5f))
				return square4;
		}
		else if(foot == "FootBoth")
		{
			GameObject leftSquare = getCurrentSquare("FootLeft");
			GameObject rightSquare = getCurrentSquare("FootRight");
			
			if(leftSquare.tag == rightSquare.tag)
				return leftSquare;
		}
		
		return gameObject;
	}

	public double getResting()
	{
		return resting;
	}

	public IEnumerator startTimer()
	{
		while(timer >= 0)
		{
			yield return new WaitForSeconds (1);
			timer--;
		}

		timeFinish = true;
	}

	public int getTimeLeft()
	{
		if(timer < 0)
		{
			return 0;
		}
		return timer;
	}

	public int getJumpCount()
	{
		return jumpCount;
	}
}

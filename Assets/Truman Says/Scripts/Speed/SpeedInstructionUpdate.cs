﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeedInstructionUpdate : MonoBehaviour
{
	public SpeedWorkoutManager workoutManager;
	public GameObject StartBox, Plane;

    Text text;

    int timesRun = 0;
    public bool countdownFinish, gettingReady, isReady;
    public int readyTime = 3;

	void Awake ()
	{
		countdownFinish = false;
		gettingReady = false;
		isReady = false;

        text = GetComponent <Text> ();
        text.text = "Step in the Middle";
	}
	
	void Update ()
	{
		if(countdownFinish && isReady)
		{
			string instruction = workoutManager.getInstruction().Replace("Square1", "Red").Replace("Square2", "Green").Replace ("Square4", "Blue");
			instruction += "\nTime: " + workoutManager.getTimeLeft().ToString();

			text.text = instruction;
		}
		else if(countdownFinish && !gettingReady)
		{
			StartCoroutine(GetReady(readyTime));
		}
		else if(timesRun == 0 && StartBox.GetComponent<StartBoxManager>().getInBox())
		{
			//set resting height for jumps after countdown
			StartCoroutine(Countdown());
		}

	}

	//count down for 5 seconds
	public IEnumerator Countdown()
	{
		int seconds = 5;
		timesRun++;

		while(seconds > 0)
		{
			text.text = "Initializing: " + seconds.ToString();
			seconds--;
			yield return new WaitForSeconds (1);

		}

		countdownFinish = true;
		Destroy(Plane);

		workoutManager.setRestingFootHeight();			
	}

	public IEnumerator GetReady(int time)
	{
		gettingReady = true;

		while(time > 0)
		{
			text.text = workoutManager.getInstruction().Replace("Square1", "Red").Replace("Square2", "Green").Replace("Square4", "Blue") + "\nin " + time.ToString();
			time--;
			yield return new WaitForSeconds(1);
		}

		isReady = true;
		text.text = "";
		StartCoroutine(workoutManager.startTimer());

		gettingReady = false;
	}
}

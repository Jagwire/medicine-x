﻿using UnityEngine;
using System.Collections;

public class FlipTopDown : MonoBehaviour {

//	flip top down camera on y-axis

	void OnPreCull()
	{
		GetComponent<Camera>().ResetWorldToCameraMatrix();
		GetComponent<Camera>().ResetProjectionMatrix();
		GetComponent<Camera>().projectionMatrix = GetComponent<Camera>().projectionMatrix * Matrix4x4.Scale(new Vector3(1,-1,1));
	}

	void OnPreRender()
	{
		GL.SetRevertBackfacing(true);
	}

	void OnPostRender()
	{
		GL.SetRevertBackfacing(false);
	}
}

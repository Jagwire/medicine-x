﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InstructionUpdate : MonoBehaviour
{
	public WorkoutManager workoutManager;
	public GameObject StartBox, Plane;
	
    Text text;

    int timesRun = 0;
    bool countdownFinish = false;

	int currentInstruction;

	void Awake ()
	{
        text = GetComponent <Text> ();
        text.text = "Step in the Middle";
	}
	
	void Update ()
	{
		//only count down and initialize first time
		if(timesRun == 0 && StartBox.GetComponent<StartBoxManager>().getInBox())
		{
			timesRun++;

			//set resting height for jumps after countdown
			StartCoroutine(Countdown());
		}
		//after count down, begin offering instructions
		else if(timesRun > 0 && countdownFinish)
		{

			//get current instruction (ex: FootRight Square3)
			string instruction = workoutManager.getInstruction();

			//translate from GameObject tag naming convention to more understandable wording
			switch(instruction)
			{
				case "FootLeft Square1":
					text.text = "Left Foot\nRed Square";
					break;
				case "FootLeft Square2":
					text.text = "Left Foot\nBlue Square";
					break;
				case "FootLeft Square3":
					text.text = "Left Foot\nGreen Square";
					break;
				case "FootLeft Square4":
					text.text = "Left Foot\nYellow Square";
					break;
				case "FootRight Square1":
					text.text = "Right Foot\nRed Square";
					break;
				case "FootRight Square2":
					text.text = "Right Foot\nBlue Square";
					break;
				case "FootRight Square3":
					text.text = "Right Foot\nGreen Square";
					break;
				case "FootRight Square4":
					text.text = "Right Foot\nYellow Square";
					break;
				default:
					text.text = instruction;
					break;
			}
		}

	}

	//count down for 5 seconds
	public IEnumerator Countdown()
	{
		int seconds = 5;

		while(seconds > 0)
		{
			text.text = "Initializing: " + seconds.ToString();
			seconds--;
			yield return new WaitForSeconds (1);

		}

		countdownFinish = true;
		text.text = "";
		Destroy(Plane);

		workoutManager.setRestingFootHeight();
	}

}

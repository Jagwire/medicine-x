﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SquareTextUpdate : MonoBehaviour {

	public SquareController square1;
	public SquareController square2;
	public SquareController square3;
	public SquareController square4;

	Text text;

	void Awake ()
    {
        text = GetComponent <Text> ();
    }

	//show which square each foot is currently in	
	void Update ()
	{
		string fl = "none", fr = "none";
		if(square1.getLeftIn())
		{
			fl = "Square1";
		}
		else if(square2.getLeftIn())
		{
			fl = "Square2";
		}
		else if(square3.getLeftIn())
		{
			fl = "Square3";
		}
		else if(square4.getLeftIn())
		{
			fl = "Square4";
		}

		if(square1.getRightIn())
		{
			fr = "Square1";
		}
		else if(square2.getRightIn())
		{
			fr = "Square2";
		}
		else if(square3.getRightIn())
		{
			fr = "Square3";
		}
		else if(square4.getRightIn())
		{
			fr = "Square4";
		}

		text.text =
		"FootLeft: " + fl + 
		"\nFootRight: " + fr;
	}
}

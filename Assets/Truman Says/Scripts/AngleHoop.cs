﻿using UnityEngine;
using System.Collections;

public class AngleHoop : MonoBehaviour {

	public GameObject lockJoint;
	public MemoryWorkoutManager workoutMaganager;
	public float size;
	float currentAngle;
	bool balanced;

	public float minAngle;
	public float maxAngle;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 position = new Vector3 ();
		position = lockJoint.GetComponent<Transform> ().position;
		GetComponent<Transform> ().position = position;

		currentAngle = (float)workoutMaganager.checkBalance ();

		if(currentAngle <= minAngle)
		{
			GetComponent<Transform>().localScale = new Vector3(.2f * size, 0.001f,.2f * size);
			balanced = true;
			return;
		}

		if(currentAngle > maxAngle)
		{
			GetComponent<Transform>().localScale = new Vector3(size, 0.001f, size);
			balanced = false;
			return;
		}

		float newScale = currentAngle / maxAngle;
		GetComponent<Transform>().localScale = new Vector3(newScale * size, 0.001f, newScale * size);
	}

	public bool isBalanced ()
	{
		return balanced;
	}
}

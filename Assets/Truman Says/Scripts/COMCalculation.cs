﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class COMCalculation : MonoBehaviour {
	
	public ParameterPreserver parameters;
	
	//joints
	public Transform leftAnkle;
	public Transform rightAnkle;
	public Transform spine;
	public Transform hipCenter;
	public Transform rightKnee;
	public Transform leftKnee;
	public Transform rightHip;
	public Transform leftHip;
	public Transform head;
	public Transform shoulderCenter;
	public Transform RshoulderCenter;
	public Transform LshoulderCenter;
	public Transform LEl;
	public Transform REl;
	public Transform RWr;
	public Transform LWr;
	public Transform RHand;
	public Transform LHand;
	public Transform RFoot;
	public Transform LFoot;
	
	//double Subject Hight
	public double height;
	
	//double Subject Weight
	public double weight;
	
	//Subject mass
	public double mass;
	
	//Subject Volume
	public double volume;
	
	//Mass of Body Segments
	double mHead;
	double mNeck;
	double mThx;
	double mAbd;
	double mPel;
	double mRTh;
	double mLTh;
	double mRCf;
	double mLCf;
	double mRFt;
	double mLFt;
	double mRFa;
	double mLFa;
	double mRH;
	double mLH;
	double mRUparm;
	double mLUparm;
	
	//CoM coordinates
	public double CoMX;
	public double CoMY;
	public double CoMZ;
	
	// Use this for initialization
	void Start ()
	{
		//Getting height and weight from the text boxes
		height = parameters.height;
		weight = parameters.weight;
		
		btnHW_Click ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		CoM ();
	}
	
	//Center of Mass calculation goes here
	private void CoM()
	{
		
		//Left Ankle Coordinates
		double leftAnkleX = leftAnkle.position.x;
		double leftAnkleY = leftAnkle.position.y;
		double leftAnkleZ = leftAnkle.position.z;
		
		//Right Ankle Coordinates
		double rightAnkleX = rightAnkle.position.x;
		double rightAnkleY = rightAnkle.position.y;
		double rightAnkleZ = rightAnkle.position.z;
		
		//Spine Coordinates
		double spineX = spine.position.x;
		double spineY = spine.position.y;
		double spineZ = spine.position.z;
		
		//Hip Centre Coordinates
		double hipCenterX = hipCenter.position.x;
		double hipCenterY = hipCenter.position.y;
		double hipCenterZ = hipCenter.position.z;
		
		//Right Knee Coordinates
		double rightKneeX = rightKnee.position.x;
		double rightKneeY = rightKnee.position.y;
		double rightKneeZ = rightKnee.position.z;
		
		//Left Knee Coordinates
		double leftKneeX = leftKnee.position.x;
		double leftKneeY = leftKnee.position.y;
		double leftKneeZ = leftKnee.position.z;
		
		//Right Hip Coordinates
		double rightHipX = rightHip.position.x;
		double rightHipY = rightHip.position.y;
		double rightHipZ = rightHip.position.z;
		
		//Left Hip Coordinates
		double leftHipX = leftHip.position.x;
		double leftHipY = leftHip.position.y;
		double leftHipZ = leftHip.position.z;
		
		//Head Coordinates
		double headX = head.position.x;
		double headY = head.position.y;
		double headZ = head.position.z;
		
		//Shoulder Centre Coordinates
		double shoulderCenterX = shoulderCenter.position.x;
		double shoulderCenterY = shoulderCenter.position.y;
		double shoulderCenterZ = shoulderCenter.position.z;
		
		//Right Shoulder Centre Coordinates
		double RshoulderCenterX = RshoulderCenter.position.x;
		double RshoulderCenterY = RshoulderCenter.position.y;
		double RshoulderCenterZ = RshoulderCenter.position.z;
		
		//Left Shoulder Center Coordinates
		double LshoulderCenterX = LshoulderCenter.position.x;
		double LshoulderCenterY = LshoulderCenter.position.y;
		double LshoulderCenterZ = LshoulderCenter.position.z;
		
		//Left Elbow Center Coordinates
		double LElX = LEl.position.x;
		double LElY = LEl.position.y;
		double LElZ = LEl.position.z;
		
		//Right Elbow Center Coordinates
		double RElX = REl.position.x;
		double RElY = REl.position.y;
		double RElZ = REl.position.z;
		
		//Right Wrist Center Coordinates
		double RWrX = RWr.position.x;
		double RWrY = RWr.position.y;
		double RWrZ = RWr.position.z;
		
		
		//Left Wrist Center Coordinates
		double LWrX = LWr.position.x;
		double LWrY = LWr.position.y;
		double LWrZ = LWr.position.z;
		
		
		//Right Hand Center Coordinates
		double RHandX = RHand.position.x;
		double RHandY = RHand.position.y;
		double RHandZ = RHand.position.z;
		
		//Left Hand Center Coordinates
		double LHandX = LHand.position.x;
		double LHandY = LHand.position.y;
		double LHandZ = LHand.position.z;
		
		//Right Foot Center Coordinates
		double RFootX = RFoot.position.x;
		double RFootY = RFoot.position.y;
		double RFootZ = RFoot.position.z;
		
		//Left Foot Center Coordinates
		double LFootX = LFoot.position.x;
		double LFootY = LFoot.position.y;
		double LFootZ = LFoot.position.z;
		
		
		//Start Find CoM
		CoMX = (headX * mHead + ((shoulderCenterX + headX) / 2) * mNeck + ((shoulderCenterX + spineX) / 2) * mThx + spineX * mAbd + hipCenterX * mPel + ((leftHipX + leftKneeX) / 2) * mLTh + ((rightHipX + rightKneeX) / 2) * mRTh + ((RshoulderCenterX + RElX) / 2) * mRUparm + ((LshoulderCenterX + LElX) / 2) * mLUparm + ((RWrX + RElX) / 2) * mRFa + ((LWrX + LElX) / 2) * mLFa + RHandX * mRH + LHandX * mLH + RFootX * mRFt + LFootX * mLFt + ((rightAnkleX + rightKneeX) / 2) * mRCf + ((leftAnkleX + leftKneeX) / 2) * mRCf) / (mHead + mThx + mNeck+mAbd + mPel + mLTh + mRTh + mRUparm + mLUparm+mRCf+mLCf+ mRFt+mLFt+ mLFa+mRFa+mLH+mRH);
		CoMY = (headY * mHead + ((shoulderCenterY + headY) / 2) * mNeck + ((shoulderCenterY + spineY) / 2) * mThx + spineY * mAbd + hipCenterY * mPel + ((leftHipY + leftKneeY) / 2) * mLTh + ((rightHipY + rightKneeY) / 2) * mRTh + ((RshoulderCenterY + RElY) / 2) * mRUparm + ((LshoulderCenterY + LElY) / 2) * mLUparm + ((RWrY + RElY) / 2) * mRFa + ((LWrY + LElY) / 2) * mLFa + RHandY * mRH + LHandY * mLH + RFootY * mRFt + LFootY * mLFt + ((rightAnkleY + rightKneeY) / 2) * mRCf + ((leftAnkleY + leftKneeY) / 2) * mRCf) / (mHead + mThx + mNeck + mAbd + mPel + mLTh + mRTh + mRUparm + mLUparm + mRCf + mLCf + mRFt + mLFt + mLFa + mRFa + mLH + mRH);
		CoMZ = (headZ * mHead + ((shoulderCenterZ + headZ) / 2) * mNeck + ((shoulderCenterZ + spineZ) / 2) * mThx + spineZ * mAbd + hipCenterZ * mPel + ((leftHipZ + leftKneeZ) / 2) * mLTh + ((rightHipZ + rightKneeZ) / 2) * mRTh + ((RshoulderCenterZ + RElZ) / 2) * mRUparm + ((LshoulderCenterZ + LElZ) / 2) * mLUparm + ((RWrZ + RElZ) / 2) * mRFa + ((LWrZ + LElZ) / 2) * mLFa + RHandZ * mRH + LHandZ * mLH + RFootZ * mRFt + LFootZ * mLFt + ((rightAnkleZ + rightKneeZ) / 2) * mRCf + ((leftAnkleZ + leftKneeZ) / 2) * mRCf) / (mHead + mThx + mNeck + mAbd + mPel + mLTh + mRTh + mRUparm + mLUparm + mRCf + mLCf + mRFt + mLFt + mLFa + mRFa + mLH + mRH);
	}
	
	//Calculate body segment masses----- calculate this on button click!
	private void btnHW_Click()
	{	
		
		//Calculate masses for females
		if (parameters.gender)
		{
			
			double pelvisV = 7.26 * (weight) - 15.13 * height + 555.63;
			double abdV = 2.002 * weight - 17.63 * height + 1008.7;
			double thxV = 7.34 * weight - .19 * height + 86.47;
			double neckV = .073 * weight + 1.55 * height - 63.89;
			double headV = .27 * weight - .19 * height + 211.61;
			
			double RthiV = 3.98 * weight + 9 * height - 519.57;
			double LthiV = 3.98 * weight + 9 * height - 519.57;
			double RcalfV = 1.13 * weight + 1.54 * height - 65.67;
			double LcalfV = 1.13 * weight + 1.54 * height - 65.67;
			double RfootV = 0.09 * weight + 1.37 * height - 58.69;
			double LfootV = 0.09 * weight + 1.37 * height - 58.69;
			double RuparmV = 0.76 * weight + 0.26 * height - 28.91;
			double LuparmV = 0.76 * weight + 0.26 * height - 28.91;
			double RforearmV = 0.36 * weight + .08 * height - .19;
			double LforearmV = 0.36 * weight + .08 * height - .19;
			double RhandV = .06 * weight + .22 * height - 1.11;
			double LhandV = .06 * weight + .22 * height - 1.11;
			
			//Body volume
			volume = pelvisV + abdV + thxV + neckV + headV + RthiV + LthiV + RcalfV + LcalfV + RfootV + LfootV + RuparmV + LuparmV + RforearmV + LforearmV + RhandV + LhandV;
			//volume in m3
			double volumeM3 = volume * (.000016387);
			double bodyDensity = mass / volumeM3;
			double density = weight / volume;
			//lbTest.Text = bodyDensity.ToString();
			
			//segment Mass = segment volume*density
			mHead = headV * density;
			mNeck = neckV * density;
			mThx = thxV * density;
			mAbd = abdV * density;
			mPel = pelvisV * density;
			mRTh = RthiV * density;
			mLTh = LthiV * density;
			mRUparm = RuparmV * density;
			mLUparm = LuparmV * density;
			mRCf = RcalfV * density;
			mLCf = LcalfV * density;
			mRFt = RfootV * density;
			mLFt = LfootV * density;
			mRFa = RforearmV * density;
			mLFa = LforearmV * density;
			mRH = RhandV * density;
			mLH = LhandV * density;
		}
		else
		{
			double pelvisV = 6.039 * (weight) - 10.69 * height + 413.002;
			double abdV = 1.184 * weight - 3.565 * height + 190.10;
			
			double thxV = 9.983 * weight - 3.47 * height + 30.16;
			double neckV = .385 * weight -0.8500 * height - 57.40;
			double headV = .125 * weight + 1.05 * height + 171.39;
			
			double RthiV = 2.79 * weight + 4.77 * height - 214.339;
			double LthiV = 2.79 * weight + 4.77 * height - 214.339;
			double RcalfV = .724 * weight + 4.42 * height - 198.27;
			double LcalfV = .724 * weight + 4.42 * height - 198.27;
			
			double RfootV = 0.128 * weight + 1.37 * height - 58.69;
			double LfootV = 0.128 * weight + 1.37 * height - 58.69;
			
			double RuparmV = 0.85 * weight - 0.789 * height - 28.28;
			double LuparmV = 0.85 * weight + 0.789 * height - 28.28;
			
			double RforearmV = 0.47 * weight - .23 * height +17.84;
			double LforearmV = 0.47 * weight - .23 * height +17.84;
			
			double RhandV = .08 * weight + .29 * height - 4.42;
			double LhandV = .08 * weight + .29 * height - 4.42;
			//Body volume
			volume = pelvisV + abdV + thxV + neckV + headV + RthiV + LthiV + RcalfV + LcalfV + RfootV + LfootV + RuparmV + LuparmV + RforearmV + LforearmV + RhandV + LhandV;
			//volume in m3
			double volumeM3 = volume * (.000016387);
			double bodyDensity = mass / volumeM3;
			double density = weight / volume;
			//lbTest.Text = bodyDensity.ToString();
			
			// Mass = volume*density
			mHead = headV * density;
			mNeck = neckV * density;
			mThx = thxV * density;
			mAbd = abdV * density;
			mPel = pelvisV * density;
			mRTh = RthiV * density;
			mLTh = LthiV * density;
			mRUparm = RuparmV * density;
			mLUparm = LuparmV * density;
			
			mLCf = LcalfV * density;
			mRFt = RfootV * density;
			mLFt = LfootV * density;
			mRFa = RforearmV * density;
			mLFa = LforearmV * density;
			mRH = RhandV * density;
			mLH = LhandV * density;
		}
		
	}
	
	/*
	//Median filter at the end to smooth the CoM measurements
	List<double> lMedian = new List<double>(); //List to help median filtering

	private void filterCOM(object sender)
	{
		for (int i = 0; i < comX.Count - 1; i++)
		{
			if (i > 0)
			{
				lMedian.Add(comX[i - 1]);
				lMedian.Add(comX[i]);
				lMedian.Add(comX[i + 1]);
				
				lMedian.Sort();
				comX[i] = lMedian[1];
				lMedian.Clear();
				
				lMedian.Add(comZ[i - 1]);
				lMedian.Add(comZ[i]);
				lMedian.Add(comZ[i + 1]);
				
				lMedian.Sort();
				comZ[i] = lMedian[1];
				lMedian.Clear();
				
				lMedian.Add(comY[i - 1]);
				lMedian.Add(comY[i]);
				lMedian.Add(comY[i + 1]);
				
				lMedian.Sort();
				comY[i] = lMedian[1];
				lMedian.Clear();
				
			}
		}
	}
*/
	
}


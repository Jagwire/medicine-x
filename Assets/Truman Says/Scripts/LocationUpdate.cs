﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class LocationUpdate : MonoBehaviour
{
//	update location text field on canvas

	public Transform spineBase;
	public Transform SpineShoulder;
	public Transform footLeft;
	public Transform footRight;
	public Transform handLeft;
	public Transform handRight;

	public MemoryWorkoutManager manager;

    Text text;

	ParameterPreserver parameters;

	void Awake ()
    {
        text = GetComponent <Text> ();

    }

	void Update ()
	{

		/*
		text.text = 
		"SpineBase: " + spineBase.position.ToString() + "\n" + 
		"SpineShoulder: " + SpineShoulder.position.ToString() + "\n" +
		"FootLeft: " + footLeft.position.ToString() + "\n" + 
		"FootRight: " + footRight.position.ToString() + "\n" + 
		"HandLeft: " + handLeft.position.ToString() + "\n" + 
		"HandRight: " + handRight.position.ToString() + "\n" +
		"currentSquare: " + manager.getCurrentSquare("FootLeft");
		*/
	}

	public void setText(string newText)
	{
		text.text = newText;
	}
}

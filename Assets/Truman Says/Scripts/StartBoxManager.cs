﻿using UnityEngine;
using System.Collections;

public class StartBoxManager : MonoBehaviour {

	//check if SpineBase is within the startbox

	bool inBox = false;

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("SpineBase"))
		{
			inBox = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("SpineBase"))
		{
			inBox = false;
		}
	}

	public bool getInBox()
	{
		return inBox;
	}
}

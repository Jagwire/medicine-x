﻿using UnityEngine;
using System.Collections;

public class SquareController : MonoBehaviour {

	public SpotlightManager spotlight;

	bool leftIn;
	bool rightIn;

	public bool lightOn;
	public bool soundOn;

	//Animator squareAnimator;

	void Awake ()
	{
		leftIn = false;
		rightIn = false;

		//squareAnimator = GetComponent<Animator> ();
	}

	void Update()
	{
		/*
		if(getLeftIn() || getRightIn())
			squareAnimator.SetBool ("inSquare", true);
		else
			squareAnimator.SetBool ("inSquare", false);
		*/
	}

	//determine which foot entered square
	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("FootLeft"))
		{
            /*
			if(lightOn)
				spotlight.illuminateWhile(gameObject, true);
			if(soundOn)
				playSound();
            */
			leftIn = true;
		}
		if(other.CompareTag("FootRight"))
		{
            /*
			if(lightOn)
				spotlight.illuminateWhile(gameObject, true);
			if(soundOn)
				playSound();
            */
			rightIn = true;
		}

	}
	
	//determine which foot left square
	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("FootLeft"))
		{
			if(lightOn)
				spotlight.illuminateWhile(gameObject, false);
			leftIn = false;
		}
		if(other.CompareTag("FootRight"))
		{
			if(lightOn)
				spotlight.illuminateWhile(gameObject, false);
			rightIn = false;
		}

	}

	public void playSound()
	{
		GetComponent<AudioSource> ().Play ();
	}

	public bool getLeftIn()
	{
		return leftIn;
	}

	public bool getRightIn()
	{
		return rightIn;
	}
}

﻿using UnityEngine;
using System.Collections;

public class ModeSelector : MonoBehaviour {
	public void loadMemoryGame() {
		Application.LoadLevel (3);
	}

	public void loadSpeedGame() {
		Application.LoadLevel (4);
	}

	public void back(){
		Application.LoadLevel (0);
	}

    public void exit()
    {
        Application.Quit();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MemoryLevelSelector : MonoBehaviour {

	public ParameterPreserver parameters;
	public Toggle leftToggle, rightToggle, bothToggle, safeToggle, easyToggle;

	public void loadLevelOne(){
		parameters.safe = safeToggle.isOn;
        parameters.easy = easyToggle.isOn;
		parameters.level = 1;

		if (bothToggle.isOn) {
			parameters.leftFoot = false;
			parameters.rightFoot = false;
		}
		else {
			parameters.leftFoot = leftToggle.isOn;
			parameters.rightFoot = rightToggle.isOn;
		}

		Application.LoadLevel (5);
	}

	public void loadLevelTwo(){
		parameters.safe = safeToggle.isOn;
        parameters.easy = easyToggle.isOn;
        parameters.level = 2;
		
		if (bothToggle.isOn) {
			parameters.leftFoot = false;
			parameters.rightFoot = false;
		}
		else {
			parameters.leftFoot = leftToggle.isOn;
			parameters.rightFoot = rightToggle.isOn;
		}
		
		Application.LoadLevel (5);
	}

	public void back(){
		Application.LoadLevel (2);
	}

}

﻿using UnityEngine;
using System.Collections;

public class RockPlatformControl : MonoBehaviour {

	public float speed = 4;
	public bool ifGameOn = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (ifGameOn) 
		{
			transform.Translate (Vector3.right * Time.deltaTime * speed);
		}
	}
	
	void Disable (bool game) 
	{
		ifGameOn = game;
	}
}

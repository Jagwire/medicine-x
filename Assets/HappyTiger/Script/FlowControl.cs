﻿using UnityEngine;
using System.Collections;

public class FlowControl : MonoBehaviour {
	
	public GameObject rockplatform;
	public GameObject coins;
	public GameObject columns;
	public GameObject background;
	public GameObject FirstBG;
	
	public float speed = 4;
	private Quaternion originBackgroundrotation;
	private Vector3 PrevRPpose1;
	private Vector3 PrevRPpose2;

	// Use this for initialization
	void Start () 
	{
		originBackgroundrotation = FirstBG.transform.rotation;
		PrevRPpose1 = rockplatform.transform.position;
		PrevRPpose2 = rockplatform.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey ("escape")) 
		{
			Application.Quit ();
		}
		float dx1 = PrevRPpose1.x - rockplatform.transform.position.x;
		if (dx1 < -8.0f) 
		{
			PrevRPpose1 = rockplatform.transform.position;
			Instantiate(coins, new Vector3(-14f, 2, 0), Quaternion.identity);
			Instantiate(columns, new Vector3(-20f, 0, 0), Quaternion.identity);
		}

		float dx2 = PrevRPpose2.x - rockplatform.transform.position.x;
		//Debug.Log ("dx2: " + dx2);
		if (dx2 < - 50) 
		{
			PrevRPpose2 = rockplatform.transform.position;
			Instantiate(background, new Vector3(-50, 5, -6), originBackgroundrotation);
		}
	}
}

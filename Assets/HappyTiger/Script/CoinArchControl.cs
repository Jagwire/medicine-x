﻿using UnityEngine;
using System.Collections;

public class CoinArchControl : MonoBehaviour {
	
	public float speed = 4;
	public bool ifGameOn = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (ifGameOn) 
		{
			transform.Translate (Vector3.right * Time.deltaTime * speed);
		}
		if (this.transform.position.x > 20) 
		{
			Destroy (this.gameObject);
		}	
	}

	void Disable (bool game) 
	{
		ifGameOn = game;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TigerControl : MonoBehaviour {
	// Use this for initialization

	public GameObject angletext;
	public GameObject leg;
	private double preangle;
	private double curangle;


	private Quaternion originRotation;
	void Start () 
	{
		preangle = 0;
		curangle = 0;
		originRotation = transform.rotation;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.rotation = originRotation;
		bool up;
		HoughControl legtrack = leg.GetComponent<HoughControl> ();
		curangle = legtrack.angle;

		GameObject g = GameObject.FindGameObjectWithTag ("AngleText"); 
		g.SendMessage("GetAngle", curangle);

		up = Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKey (KeyCode.UpArrow);

		if (up == false) 
		{
			if (curangle > 20 && preangle < 20) 
			{
				up = true;
				preangle = curangle;
			} 
			else 
			{
				preangle = curangle;
				up = false;
			}
		}

		if (up == true) 
		{
			//Debug.Log("upupup!");
			GetComponent<Rigidbody>().velocity = new Vector3(0, 9, 0);				
		} 
		else 
		{
			GetComponent<Rigidbody>().AddForce (Vector3.down * 2100f * Time.deltaTime);
		}
	}
}

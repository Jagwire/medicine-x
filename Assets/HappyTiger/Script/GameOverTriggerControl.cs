﻿using UnityEngine;
using System.Collections;

public class GameOverTriggerControl : MonoBehaviour {

	public float speed = 4;
	public bool ifGameOn = true;

	// Use this for initialization
	void Start () {
	
	}

	void OnCollisionEnter(Collision other) 
	{
		Debug.Log ("Collision ENTERED!");
		if (other.gameObject.name == "Tiger") 
		{
			Debug.LogError("GAME OVER!!!");
		} 

			string[] movabletags = new string[]{"CoinsArch", "ColumnArray", "RockPlatform", "Background", "ScoreCounter"};

		foreach (string tagstr in movabletags) 
		{
			GameObject[] gos = GameObject.FindGameObjectsWithTag (tagstr); 
			foreach (GameObject g in gos) 
			{
				g.SendMessage ("Disable", false);
			}
		}
	
	}

	void Update()
	{

	}
	
}
